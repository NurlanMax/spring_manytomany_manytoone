package com.example.demo.beans;

public class OtherBean {
    private String connectionURL;
    private int port;

    public OtherBean(String connectionURL, int port) {
        System.out.println("Using parametrized constructor of other bean");
        this.connectionURL = connectionURL;
        this.port = port;
    }

    public OtherBean() {
        System.out.println("using default constructor of other bean");
        this.connectionURL="127.0.0.1";
        this.port=8081;
    }

    public String getConnectionURL() {
        return connectionURL;
    }

    public void setConnectionURL(String connectionURL) {
        this.connectionURL = connectionURL;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getConnection(){
        return "The connection is => "+this.connectionURL+":"+this.port;
    }
}
