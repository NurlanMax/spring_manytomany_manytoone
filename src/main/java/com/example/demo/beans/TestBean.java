package com.example.demo.beans;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("prototype")
public class TestBean {
    private String name;
    private int price;

    public TestBean(){
        System.out.println("Creating first Test Bean");
        this.name="Ilyas";
        this.price=12;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }
    public String getTextData(){
        return this.name+" - " +this.price;
    }

}
