package com.example.demo.controllers;

import com.example.demo.beans.OtherBean;
import com.example.demo.beans.TestBean;
import com.example.demo.db.DBManager;
import com.example.demo.entities.Country;
import com.example.demo.entities.Food;
import com.example.demo.entities.Ingredient;
import com.example.demo.entities.Item;
import com.example.demo.repo.CountryRepo;
import com.example.demo.repo.FoodRepository;
import com.example.demo.repo.IngredientRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Controller
public class HomeController {

    @Autowired
    private FoodRepository foodRepository;
    @Autowired
    private CountryRepo countryRepo;

    @Autowired
    private IngredientRepo ingredientRepo;

    @GetMapping(value = "/")
    public String indexPage(Model model) {
        List<Food> foods=foodRepository.findAllByPriceGreaterThanEqualAndCaloriesGreaterThanEqual(0,0);
//        List<Food> foods=foodRepository.findAllByNameLike("%c%");
        model.addAttribute("foods", foods);

        List<Country> countries=countryRepo.findAll();
        model.addAttribute("countries", countries);
        return "home";
    }

    @PostMapping(value = "/addfood")
    public String addFood(@RequestParam(name="name")String name,
                          @RequestParam(name="price")int price,
                          @RequestParam(name="calories") double calories,
                          @RequestParam(name = "is_halal") boolean isHalal,
                          @RequestParam(name = "country_id")Long countryId) {

        Optional<Country> opt=countryRepo.findById(countryId);

        if(opt.isPresent()) {

            Country cnt = opt.get();

            Food food = new Food();
            food.setName(name);
            food.setPrice(price);
            food.setAddedDate(new Date());
            food.setCalories(calories);
            food.setHalal(isHalal);
            food.setCountry(cnt);
            foodRepository.save(food);
        }
        return "redirect:/";
    }

    @GetMapping(value = "/detail/{id}")
    public String detail(@PathVariable(name = "id") Long id, Model model) {
        Optional<Food> opt=foodRepository.findById(id);
        if(opt.isPresent()) {
            Food food=opt.get();
            model.addAttribute("food", food);

            List<Country> countries=countryRepo.findAll();
            model.addAttribute("countries", countries);

            List<Ingredient> ingredients =ingredientRepo.findAll();

            if(food.getIngredients()!=null) {
                ingredients.removeAll(food.getIngredients());
            }

            model.addAttribute("ingredients", ingredients);
        } else {
            return "redirect:/";
        }
        return "detail";
    }
    @PostMapping(value = "/savefood")
    public String saveFood(@RequestParam(name="id") Long id,
                           @RequestParam(name = "name") String name,
                           @RequestParam(name = "price") int price,
                           @RequestParam(name = "calories") double calories,
                           @RequestParam(name = "is_halal") boolean isHalal,
                           @RequestParam(name = "country_id")Long countryId) {

        Optional<Country> cntOpt=countryRepo.findById(countryId);

        if(cntOpt.isPresent()) {

            Country cnt = cntOpt.get();

            Optional<Food> opt = foodRepository.findById(id);
            if (opt.isPresent()) {
                Food food = opt.get();
                food.setName(name);
                food.setCalories(calories);
                food.setPrice(price);
                food.setHalal(isHalal);
                food.setCountry(cnt);
                foodRepository.save(food);
                return "redirect:/detail/" + id;
            }

        }

        return "redirect:/";
    }
    @PostMapping(value = "/deletefood")
    public String deleteFood(@RequestParam(name = "id") Long id) {

        Optional<Food> opt=foodRepository.findById(id);
        if(opt.isPresent()){

            Food food=opt.get();
            foodRepository.delete(food);
        }
        return "redirect:/";
    }

    @PostMapping(value = "/pushingredients")
    public String pushIngredients(@RequestParam(name = "food_id") Long foodId,
                                  @RequestParam(name = "ingredient_id")Long ingredientId) {

        Optional<Ingredient> ingOpt=ingredientRepo.findById(ingredientId);
        if(ingOpt.isPresent()){
            Ingredient ingredient=ingOpt.get();
            Optional<Food> foodOpt=foodRepository.findById(foodId);

            if(foodOpt.isPresent()){

                Food food=foodOpt.get();

                List<Ingredient> ingredients=food.getIngredients();
                if(ingredients==null) {
                    ingredients=new ArrayList<>();
                }
                ingredients.add(ingredient);
                food.setIngredients(ingredients);
                foodRepository.save(food);

                return "redirect:/detail/"+foodId+"#ingredientsDiv";
            }
        }
        return "redirect:/";
    }

    @PostMapping(value = "/popingredients")
    public String popIngredients(@RequestParam(name = "food_id") Long foodId,
                                  @RequestParam(name = "ingredient_id")Long ingredientId) {

        Optional<Ingredient> ingOpt=ingredientRepo.findById(ingredientId);
        if(ingOpt.isPresent()){
            Ingredient ingredient=ingOpt.get();
            Optional<Food> foodOpt=foodRepository.findById(foodId);

            if(foodOpt.isPresent()){

                Food food=foodOpt.get();

                List<Ingredient> ingredients=food.getIngredients();
                if(ingredients==null) {
                    ingredients=new ArrayList<>();
                }
                ingredients.remove(ingredient);
                food.setIngredients(ingredients);
                foodRepository.save(food);

                return "redirect:/detail/"+foodId+"#ingredientsDiv";
            }
        }
        return "redirect:/";
    }


}