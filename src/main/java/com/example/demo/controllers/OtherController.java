package com.example.demo.controllers;

import com.example.demo.beans.TestBean;
import org.dom4j.rule.Mode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/admin")
public class OtherController {

    @GetMapping(value = "/main")
    public String index() {

        return "/home";
    }
    @GetMapping(value = "/test")
    public String test(Model model) {

        return "home";
    }
}
