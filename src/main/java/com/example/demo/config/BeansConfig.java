package com.example.demo.config;

import com.example.demo.beans.OtherBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeansConfig {
    @Bean
    public OtherBean otherBean(){
        return new OtherBean();
    }
    @Bean
    public OtherBean theOtherBean(){
        return new OtherBean("localHost",3306);
    }
    @Bean(name = "ilyas")
    public OtherBean wowBean(){
        return new OtherBean("wowhost",777);
    }

}
