package com.example.demo.db;

import com.example.demo.entities.Item;

import java.util.ArrayList;

public class DBManager {
    private static ArrayList<Item> items=new ArrayList<>();
    private static Long id=5L;
    static {
        items.add(new Item(1L,"IPhone",800000,"iphone_12_max"));
        items.add(new Item(2L,"Samsung",600000,"samsung_galaxy"));
        items.add(new Item(3L,"Xiaomi",350000,"xiaomo_redmi"));
        items.add(new Item(4L,"OPPO",400000,"oppo_blacks"));
    }
    public static void addItem(Item item){
        item.setId(id);
        items.add(item);
        id++;
    }
    public static ArrayList<Item> getAllItems(){
        return items;
    }
    public static Item getItem(Long id){
        for(Item it: items) {
            if(it.getId()==id) return it;
        }
        return null;
    }
}
